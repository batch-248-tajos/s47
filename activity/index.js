const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

const onTextChangeListener = () => (e) => {
    spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
}

txtFirstName.addEventListener('keyup', onTextChangeListener())
txtLastName.addEventListener('keyup', onTextChangeListener())