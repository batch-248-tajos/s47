console.log("Hi, B248!");
console.log(document); //result - html document
console.log(document.querySelector("#txt-first-name"));
/*
	document - refers to the whole web page
	querySelector - used to select a specific element (obj) as long as it is inside the html tag (HTML ELEMENT)
	-takes a string input that is formatted like CSS Selector
	-can select elements regardless if the string is an id, class, or a tag as long as the element is existing in the webpage
*/
/*
	Alternative methods that we use aside from querySelector in retrieving alements. Syntax:
	document.getElementById()
	document.getElementByClassName()
	document.getElementByTagName()
*/

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

/*
	event - actions that the user is doing in our webpage (scroll, click, hover, keypress/type)
	(event) can have a shorthand of (e)
	

	addEventListener - function that lets the webpage to listen to the events 						performed by the user
					- Allows us to let our users interact with our page. Each click or hover is an event which can trigger a function or task.
					-takes two arguments
					-string - the event to whcih the HTML element will listen, these are predetermined
					-function - executed by the element once the event (first argyment) is triggered
		Syntax:
		selectedElement.addEventListener('event', function);
			
*/

txtFirstName.addEventListener('keyup',(event)=>{

	spanFullName.innerHTML = txtFirstName.value

})

txtLastName.addEventListener('keyup', (event) => {
    spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
})

/*
	innerHTML - is a property of an element which considers all the childer of the selected element as a string.

	.value of the input text field
*/

// makes the element listen to multiple events
txtFirstName.addEventListener('keyup',(event)=>{
	console.log(event);
	console.log(event.target);
	console.log(event.target.value);
})

/*
	The "event" argument contains the information on the triggered event.
	The "event.target" contains the element where the event happened 
	The "event.target.value" gets the value of the input object (similar to the txtFirstName.value).
*/

/*
	Mini-Activity
	Make another keyup event where in the span element will record the last name in the forms
	send the output in the batch google chat
*/

/*
	Solution
*/

// txtLastName.addEventListener('keyup',(event)=>{
// 	spanFullName.innerHTML = txtLastName.value
// })